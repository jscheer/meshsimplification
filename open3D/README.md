# Preparation & Install
## Prerequisites
- [open3D](http://www.open3d.org)

## Setup Python Environment
Run from your `source` folder:
``` bash
python3 -m venv virtual_env
source virtual_env/bin/activate
pip install -r requirements.txt
```

## Start/Stop Python Environment
run from package folder:
``` bash
source virtual_env/bin/activate

# do stuff

deactivate
```

# Example
``` bash
python mesh_decimation.py
```
