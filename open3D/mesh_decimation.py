# ----------------------------------------------------------------------------
# -                        Open3D: www.open3d.org                            -
# ----------------------------------------------------------------------------
# Copyright (c) 2018-2023 www.open3d.org
# SPDX-License-Identifier: MIT
# ----------------------------------------------------------------------------

import open3d as o3d
import numpy as np


def filterTriangleMesh(mesh):
    print(f"\tfilter Mesh...")
    mesh = mesh.remove_degenerate_triangles()
    mesh = mesh.remove_duplicated_triangles()
    mesh = mesh.remove_duplicated_vertices()
    mesh = mesh.remove_non_manifold_edges()
    mesh = mesh.remove_unreferenced_vertices()

    mesh = o3d.t.geometry.TriangleMesh.from_legacy(mesh).fill_holes().to_legacy()
    
    #triangleData = np.asarray(mesh.triangles)
    #triangleCount = np.shape(triangleData)[0]
    #print(f"\tfinal Mesh has {triangleCount} triangles")

    return [mesh, triangleCount]


if __name__ == "__main__":

    # Define in-out filenames
    DATA_PATH = "../data"
    OUT_PATH = f"{DATA_PATH}/open3D"
    inFile = "typewriter"

    inFullFile = f"{DATA_PATH}/{inFile}.ply"
    decimatedFullFile = f"{OUT_PATH}/{inFile}_decimated.ply"


    # Read mesh and number of triangles:
    triangleMesh = o3d.io.read_triangle_mesh(inFullFile)

    triangleData = np.asarray(triangleMesh.triangles)
    triangleCount = np.shape(triangleData)[0]

    print(f"\nInput Mesh ({inFullFile}) has {triangleCount} triangles")

    # Decimate Mesh
    decFactor = 0.3
    targetCount = int(triangleCount * decFactor)
    print(f"\tdecimate to {targetCount}")

    decimatedMesh = triangleMesh.simplify_quadric_decimation(target_number_of_triangles=targetCount)
    o3d.io.write_triangle_mesh(decimatedFullFile, decimatedMesh)

    # Visualize
    # o3d.visualization.draw_geometries([triangleMesh])

    # Clean up Artifacts
    # triangleMesh = o3d.io.read_triangle_mesh(inFullFile)
    filteredFullFile = f"{OUT_PATH}/{inFile}_filtered.ply"
    [filteredMesh, numTriangles] = filterTriangleMesh(decimatedMesh)

    o3d.io.write_triangle_mesh(filteredFullFile, filteredMesh)
