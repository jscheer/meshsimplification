# MeshSimplification
This repo looks into various 3D mesh simplification methods.

## Method Overview
- open3D: [simplify_quadric_decimation](http://www.open3d.org/docs/release/python_api/open3d.geometry.TriangleMesh.html#open3d.geometry.TriangleMesh.simplify_quadric_decimation) <br>
  implements the `Quadric Error Metric Decimation` by `Garland and Heckbert`

## Getting started
- Download resources: <br>
  from top-level directory:
  ``` bash
  wget https://cloud.jscheer.de/index.php/s/A2EFFF8S7CEYcww/download/typewriter.ply -O ./data/typewriter.ply
  ```

# Notes
## Update requirements
Each project is self contained. Dependencies are stored within the `requirements.txt` files.<br>

### pipreqs
We use `pipreqs` to generate `requirements.txt` files. <br>
You can install `pipreqs` as follows:
``` bash
pip install pipreqs
# or: python3 -m pip install pipreqs
```
<br>
Updating the `requirements.txt` files can be done by:
``` bash
pipreqs <path_to_src_folder> --force
```

# Examples
Examples and results can be found [here](https://cloud.jscheer.de/index.php/s/zcBDan87CptSzZM).
